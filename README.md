# 🏁🏴 REST Countries API with color theme switcher workshop 🏴🏁

### ⚙ Technologies ⚙
- [ReactJS](https://react.dev)
- [Typescript](https://www.typescriptlang.org)
- [Tailwind CSS](https://tailwindcss.com)
- [React Router](https://reactrouter.com/en/main)
- [React Hook Form](https://react-hook-form.com)
- [Axios](https://axios-http.com/docs/intro)
- [REST Countries API](https://restcountries.com) v2

### What's Next:
- [x] UPGRADE React version from 16.9 to 18.x
- [x] UPGRADE Tailwind version from 1 to 3.x
- [x] FIX errors about Accessibility
- [ ] IMPORT stuffs by Absolute Path
- [ ] UPGRADE React Router version from 5 to 6.x
- [ ] UPGRADE React Hook Form version from 6 to 7.x
- [ ] ENHANCE it as Progressive Web App
  - [ ] Support *offline* mode
    - [ ] Search when offline (without invoking APIs)
- [ ] UTILIZE REST Countries API v3.x instead v2 *(OR FIND solution in REST Countries API new version IF it can use parameters more than 1 param)*
- [ ] IMPROVE Select/Options and Input components
  - [ ] Select/option component
    - [x] Fix placing center for Chevron down icon on Select component
    - [ ] Option List should not default by Operating System. (It doesn't look good)
    - [ ] Input Search component (Search icon is not placed center)

- [ ] IMPLEMENT with [SWR](https://swr.vercel.app) or [React Query](https://react-query-v3.tanstack.com)
- [ ] IMPLEMENT Filter by Select/Options and Input by Intersection result
- [ ] IMPLEMENT infinite scrolling on Country List page

### 🏁 About this challenge 🏁
[Frontend Mentor - Countries challenge](https://www.frontendmentor.io/challenges/rest-countries-api-with-color-theme-switcher-5cacc469fec04111f7b848ca)  
- [My Submission](https://www.frontendmentor.io/solutions/rest-countries-api-with-color-theme-switcher-tOKMs3bW9v)  
- [Preview Site](https://countries-workshop.vercel.app)  

#### 📷 Requirement Pics 🖼
![Desktop Preview](./rest-countries-api-with-color-theme-switcher-master/design/desktop-preview.jpg)  

![Desktop List Light](./rest-countries-api-with-color-theme-switcher-master/design/desktop-design-home-light.jpg)  

![Desktop List Dark](./rest-countries-api-with-color-theme-switcher-master/design/desktop-design-home-dark.jpg)  

![Desktop Detail Light](./rest-countries-api-with-color-theme-switcher-master/design/desktop-design-detail-light.jpg)  

![Desktop Detail Dark](./rest-countries-api-with-color-theme-switcher-master/design/desktop-design-detail-dark.jpg)    

![Mobile List Light](./rest-countries-api-with-color-theme-switcher-master/design/mobile-design-home-light.jpg)        

![Mobile List Dark](./rest-countries-api-with-color-theme-switcher-master/design/mobile-design-home-dark.jpg)          

![Mobile Detail Light](./rest-countries-api-with-color-theme-switcher-master/design/mobile-design-detail-light.jpg)    

![Mobile Detail Dight](./rest-countries-api-with-color-theme-switcher-master/design/mobile-design-detail-dark.jpg)     
