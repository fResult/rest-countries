import React, { ChangeEventHandler, FormEventHandler } from 'react'
import { UseFormMethods } from 'react-hook-form'
import InputField from '../InputField'
import SelectField from '../SelectField'
import './filter-form.css'

type FilterFormProps = {
  className?: string
  onSubmit: FormEventHandler<HTMLFormElement>
  register: UseFormMethods['register']
  onChangeSearchField: ChangeEventHandler<HTMLInputElement>
  onChangeRegion: ChangeEventHandler<HTMLSelectElement>
  countryName?: string
  selectedRegion?: string
}

function FilterForm({
  className,
  onSubmit,
  register,
  onChangeSearchField,
  onChangeRegion,
  countryName,
  selectedRegion
}: FilterFormProps) {
  return (
    <form className={`form--filter-countries ${className}`} onSubmit={onSubmit}>
      <div className="flex flex-col lg:flex-row lg:justify-between gap-y-12 lg:gap-y-0">
        <InputField
          id="input-search-country"
          name="search-country"
          label="Country Name"
          prefix={<i className="fas fa-search" />}
          placeholder="Search for a country..."
          type="search"
          register={register}
          inputStyle={{ width: '100%', maxWidth: 480 }}
          onChange={onChangeSearchField}
          value={countryName}
        />
        <SelectField
          id="select-region"
          name="region-selection"
          label="Region"
          className={`input-select-region ${
            selectedRegion ? 'dark:text-light-text' : ''
          }`}
          register={register}
          onChange={onChangeRegion}
          selectStyle={{}}
          value={selectedRegion}
          options={[
            { value: '', label: 'Filter by region...' },
            { value: 'Africa', label: 'Africa' },
            { value: 'Americas', label: 'America' },
            { value: 'Asia', label: 'Asia' },
            { value: 'Europe', label: 'Europe' },
            { value: 'Oceania', label: 'Oceania' }
          ]}
        />
      </div>
      <input type="submit" className="w-0 h-0" />
    </form>
  )
}

export default FilterForm
