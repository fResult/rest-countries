import React from 'react'

type CardProps = {
  className?: string
  style?: React.CSSProperties
  children: React.ReactNode
}

function Card(props: CardProps) {
  const { className = '', style, children } = props
  return (
    <div
      className={`${className} bg-light-elem dark:bg-dark-elem rounded-lg overflow-hidden shadow-lg hover:shadow-xl cursor-pointer`}
      style={style}
    >
      {children}
    </div>
  )
}

export default Card
