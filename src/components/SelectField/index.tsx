import React, { ChangeEventHandler } from 'react'
import { UseFormMethods } from 'react-hook-form'
import './select-field.css'
import { EMPTY_FUNCTION } from '../../utils'

type SelectFieldProps = {
  id?: string
  className?: string
  selectStyle?: React.CSSProperties
  name?: string
  label?: string
  onChange: ChangeEventHandler<HTMLSelectElement>
  register?: UseFormMethods['register']
  options?: Array<OptionProps>
  value?: string
}

type OptionProps = {
  value: string
  label: string
}

function SelectField(props: SelectFieldProps) {
  const {
    id = '',
    className = '',
    selectStyle = {},
    name = '',
    label = '',
    onChange = EMPTY_FUNCTION,
    register = EMPTY_FUNCTION,
    options = [],
    value = ''
  } = props

  return (
    <div className={`select-wrap ${className} relative`}>
      {label && (
        <label htmlFor={id} className={`field-label--${name}`}>
          {label}
        </label>
      )}
      <select
        ref={register}
        className={`field-select--${name} px-6 bg-light-elem dark:bg-dark-elem text-light-text dark:text-dark-text shadow-md rounded-md ${
          className ? className : ''
        }`}
        {...{
          id,
          name,
          style: { height: 55, ...selectStyle },
          'aria-label': label,
          onChange,
          value
        }}
      >
        {options?.map((opt: OptionProps) => (
          <option key={opt.value} value={opt.value}>
            {opt.label}
          </option>
        ))}
      </select>
      <i className="absolute fas fa-chevron-down top-[calc(55px-50%)] right-6 -translate-y-1/2 dark:text-dark-text" />
    </div>
  )
}

export default SelectField
