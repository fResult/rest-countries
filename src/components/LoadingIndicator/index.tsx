import React from 'react'

function LoadingIndicator() {
  return (
    <div className="w-screen h-screen fixed block inset-0 bg-white opacity-75 z-50">
      <div
        className="m-auto text-light-text dark:text-dark-text opacity-75 top-1/2 my-0 mx-auto block relative w-0 h-0"
        style={{ top: '50%' }}
      >
        <i className="fas fa-circle-notch fa-spin fa-5x" />
      </div>
    </div>
  )
}

export default LoadingIndicator
