import React, { useContext } from 'react'
import { ThemeContext } from '../../contexts/ThemeContext'
import { Link } from 'react-router-dom'
import { capitalize, Theme } from '../../utils'

const headerLinks = [
  {
    id: 1,
    text: 'Coded By: fResult Sila',
    url: 'https://gitlab.com/fResult'
  },
  {
    id: 2,
    text: 'Challenged By: Frontend Mentor',
    url: 'https://www.frontendmentor.io/solutions/rest-countries-api-with-color-theme-switcher-tOKMs3bW9v'
  }
]

function Header() {
  const { theme, setTheme } = useContext(ThemeContext)!

  const isDark = theme === Theme.Dark

  function handleDarkMode() {
    setTheme && setTheme(isDark ? Theme.Light : Theme.Dark)
  }

  return (
    <header
      className="header px-4 sm:px-20 text-light-text dark:text-dark-text shadow-lg"
      style={{ height: 80 }}
    >
      <nav className="flex justify-between items-center h-full">
        <h1 className="text-md sm:text-3xl">
          <Link to="/">Where in the world?</Link>
        </h1>

        <ul className="flex flex-col sm:flex-row text-center text-xs sm:text-md gap-x-6 gap-y-2">
          {headerLinks.map(({ id, text, url }) => {
            return (
              <li key={id}>
                <a
                  className="text-light-link dark:text-dark-link hover:text-black dark:hover:text-white hover:underline"
                  href={url}
                  rel="noreferrer"
                  target="_blank"
                >
                  {text}
                </a>
              </li>
            )
          })}
        </ul>

        <button
          className="cursor-pointer flex items-center"
          onClick={handleDarkMode}
        >
          <span className="mr-2.5">
            <i className={`fas fa-${isDark ? 'sun' : 'moon'} text-2xl`} />
          </span>
          <span>{capitalize(isDark ? Theme.Light : Theme.Dark)} Mode</span>
        </button>
      </nav>
    </header>
  )
}

export default Header
