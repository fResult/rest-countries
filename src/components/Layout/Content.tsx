import React from 'react'

type ContentProps = {
  children: React.ReactNode
}

function Content({ children }: ContentProps) {
  return <main className="py-8 sm:py-10 px-4 sm:px-20">{children}</main>
}

export default Content
