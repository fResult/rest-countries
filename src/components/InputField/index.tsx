import React, { ChangeEventHandler } from 'react'
import { UseFormMethods } from 'react-hook-form'
import { EMPTY_FUNCTION } from '../../utils'

type FieldProps = {
  className?: string
  inputStyle?: React.CSSProperties
  labelStyle?: React.CSSProperties
  id: string
  type?: string
  name: string
  label?: string
  prefix?: React.ReactNode
  placeholder?: string
  suffix?: React.ReactNode
  register: UseFormMethods['register']
  onChange: ChangeEventHandler<HTMLInputElement>
  value?: string
}

function InputField(props: FieldProps) {
  const {
    id = '',
    type = 'text',
    label = '',
    prefix,
    suffix,
    name = '',
    placeholder = '',
    register = EMPTY_FUNCTION,
    className = '',
    inputStyle = {},
    labelStyle = {},
    onChange = EMPTY_FUNCTION,
    value = ''
  } = props

  return (
    <div className={`input-wrap ${className}`}>
      {label && (
        <label htmlFor={id} className={`field-label--${name}`}>
          {label}
        </label>
      )}
      <div className={`${(prefix || suffix) && 'relative'}`}>
        {prefix && (
          <span
            className="mr-6 absolute top-0 left-6 dark:text-dark-text"
            style={{ ...labelStyle, transform: 'translateY(calc(50%))' }}
          >
            {prefix}
          </span>
        )}

        {suffix && (
          <span className="ml-6 absolute top-0 dark:text-dark-text">
            {suffix}
          </span>
        )}

        <input
          className={`field-input--${name} ${
            prefix ? 'px-14' : 'px-6'
          } bg-light-elem dark:bg-dark-elem rounded-md placeholder-light-input dark:placeholder-dark-text shadow-lg`}
          style={{ width: 480, height: 55, ...inputStyle }}
          {...{
            id,
            type,
            name,
            ref: register,
            'aria-label': label,
            placeholder,
            onChange,
            value
          }}
        />
      </div>
    </div>
  )
}

export default InputField
