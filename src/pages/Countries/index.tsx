import React, { ChangeEvent, useCallback, useEffect, useState } from 'react'
import InfiniteScroll from '../../components/InfiniteScroll/InfiniteScroll'
import FilterForm from '../../components/Form/FilterForm'
import Card from '../../components/Card/Card'
import { useForm } from 'react-hook-form'
import useDebounce from '../../cores/hooks/useDebounce'
import LoadingIndicator from '../../components/LoadingIndicator'
import * as ApiCountries from '../../services/APIs/countries.api'
import { AxiosError } from 'axios'
import { Link } from 'react-router-dom'

function Countries() {
  const { register, handleSubmit } = useForm<FilterCountryForm>()

  const [countryName, setCountryName] = useState('')
  const [selectedRegion, setSelectedRegion] = useState('')
  const [countries, setCountries] = useState<Array<ICountry>>([])
  const [isLoading, setIsLoading] = useState(false)
  const [error, setError] = useState('')

  const debouncedSearch = useDebounce(countryName, 750)
  const debouncedRegion = useDebounce(selectedRegion, 100)

  const handleSearchCountries = useCallback(
    async ({ searchField, region }: FilterCountryForm) => {
      try {
        setIsLoading(true)

        if (region) {
          const { data } = await ApiCountries.findByRegion(region)
          setCountries(data)
          setError('')
          return
        }

        if (searchField) {
          const { data } = await ApiCountries.findCountries({
            searchField,
            region
          })
          setCountries(data)
          setError('')
          return
        }

        if (searchField === '' && region === '') {
          const { data } = await ApiCountries.findCountries()
          setCountries(data)
          setError('')
          return
        }
      } catch (err) {
        console.error('❌ Error', err)
        if (err instanceof AxiosError<{ message: string }>) {
          setError(err?.response?.data.message || 'Error')
        } else {
          setError('Something went wrong')
        }
      } finally {
        setIsLoading(false)
      }
    },
    []
  )

  useEffect(() => {
    if (debouncedRegion || debouncedSearch || debouncedSearch === '') {
      ;(async () => {
        await handleSearchCountries({
          searchField: debouncedSearch,
          region: debouncedRegion
        })
      })()
    }
  }, [debouncedSearch, debouncedRegion, handleSearchCountries])

  useEffect(() => {}, [selectedRegion])

  function handleChangeRegion(e: ChangeEvent<HTMLSelectElement>) {
    setCountryName('')
    setSelectedRegion(e.target.value)
  }

  function handleChangeValueSearch(e: ChangeEvent<HTMLInputElement>) {
    setSelectedRegion('')
    setCountryName(e.target.value)
  }

  const renderCard = ({
    item,
    key
  }: {
    item: ICountry
    key: string | number
  }) => (
    <Link
      key={key}
      to={{
        pathname: `/countries/${key}`,
        state: { country: item, countries }
      }}
    >
      <Card
        className="mx-auto"
        style={{ width: 265, height: 335 }}
      >
        <div className="">
          <img
            style={{ height: 160, width: 265, objectFit: 'cover' }}
            src={item.flag}
            alt={item.name}
          />
        </div>
        <div className="px-4 pt-5 pb-10 text-light-text dark:text-dark-text">
          <p className="mb-4 text-xl font-bold" onClick={() => {}}>
            {item.name}
          </p>
          <p>
            <span className="label">Population:&nbsp;</span>
            <span>{item.population.toLocaleString()}</span>
          </p>
          <p>
            <span className="label">Region:&nbsp;</span>
            <span>{item.region}</span>
          </p>
          <p>
            <span className="label">Capital:&nbsp;</span>
            <span>{item.capital}</span>
          </p>
        </div>
      </Card>
    </Link>
  )
  return (
    <div className="countries">
      <FilterForm
        register={register}
        onSubmit={handleSubmit(handleSearchCountries)}
        selectedRegion={selectedRegion}
        countryName={countryName}
        onChangeSearchField={handleChangeValueSearch}
        onChangeRegion={handleChangeRegion}
      />
      {isLoading ? (
        <LoadingIndicator />
      ) : !error ? (
        <InfiniteScroll
          items={countries}
          renderEmptyList={() => (
            <div className="text-light-text dark:text-dark-text">
              No Content
            </div>
          )}
          keyExtractor={({ alpha3Code }) => alpha3Code}
          renderItem={renderCard}
          className="flex flex-col mt-8 md:flex-row md:flex-wrap gap-14 justify-between"
        />
      ) : (
        <div className="text-error text-2xl text-center mt-8">
          <span>{error}</span>
        </div>
      )}
    </div>
  )
}

export default Countries
