import React from 'react'

const Notfound = () => {
  return (
    <div className="flex justify-center items-center">
      <h2 className="text-2xl">Not Found Page</h2>
    </div>
  )
}

export default Notfound
