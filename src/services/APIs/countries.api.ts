import axios from 'axios'

const BASE_URL = 'https://restcountries.com/v2'

export async function findCountries(params?: FilterCountryForm) {
  if (params?.searchField || params?.region) {
    return await axios.get<Array<ICountry>>(`${BASE_URL}/name/${params.searchField}`)
  }
  return await axios.get<Array<ICountry>>(`${BASE_URL}/all`)
}

export async function findById(id: string) {
  return await axios.get<ICountry>(`${BASE_URL}/alpha/${id}`)
}

export async function findByRegion(region: string) {
  return await axios.get<Array<ICountry>>(`${BASE_URL}/region/${region}`)
}
