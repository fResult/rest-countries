interface IRegionalBloc {
  acronym: string
  name: string
  otherAcronyms: string[]
  otherNames: string[]
}
