interface ICurrency {
  code: string
  name: string
  symbol: string
}
