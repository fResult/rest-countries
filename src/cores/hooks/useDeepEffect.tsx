import { useCallback, useEffect, useRef } from 'react'
import { dequal } from 'dequal'

function useDeepEffect(callback: () => void, deps: Array<any>) {
  const ref = useRef<any>()
  const deepComparison = useCallback((deps: Array<any>) => {
    if (!dequal(deps, ref.current)) {
      ref.current = deps
      return ref.current
    }
  }, [])
  useEffect(callback, [callback, ...deepComparison(deps)])
}

export default useDeepEffect
