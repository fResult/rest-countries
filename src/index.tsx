import React, { StrictMode} from 'react'
import { createRoot } from 'react-dom/client'
import './index.css'
// import './tailwind.output.css'
import App from './App'
import reportWebVitals from './reportWebVitals'
// import '/public/fonts/NunitoSans-Black.ttf'
// import '/public/fonts/NunitoSans-BlackItalic.ttf'
// import '/public/fonts/NunitoSans-Bold.ttf'
// import '/public/fonts/NunitoSans-BoldItalic.ttf'
// import '/public/fonts/NunitoSans-ExtraBold.ttf'

const container = document.querySelector('#root') as HTMLDivElement
const root = createRoot(container)

root.render(
  <StrictMode>
    <App />
  </StrictMode>
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
