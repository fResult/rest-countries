import React, { useState, useLayoutEffect } from 'react'
import { Theme } from '../utils'

function getInitialTheme(): Theme {
  if (typeof window !== 'undefined' && window.localStorage) {
    const storedTheme = window.localStorage.getItem('theme')
    if (storedTheme === Theme.Dark || storedTheme === Theme.Light) {
      return storedTheme
    }

    const matchedOsTheme = window.matchMedia('(prefers-color-scheme: dark)').matches
    if (matchedOsTheme) return Theme.Dark
  }

  // If you want to use light theme as the default, return "light" instead
  return Theme.Light
}

type ThemeProviderProps = {
  initialTheme?: Theme
  children: React.ReactNode
}

type ThemeContextProps = {
  theme?: Theme
  setTheme?: React.Dispatch<React.SetStateAction<Theme>>
}

export const ThemeContext =
  React.createContext<Nullable<ThemeContextProps>>(null)

function ThemeProvider({ initialTheme, children }: ThemeProviderProps) {
  const [theme, setTheme] = useState<Theme>(getInitialTheme())

  useLayoutEffect(() => rawSetTheme(theme), [theme])

  const rawSetTheme = (theme: Theme) => {
    const root = window.document.documentElement
    const isDark = theme === Theme.Dark
    root.classList.remove(isDark ? Theme.Light : Theme.Dark)
    root.classList.add(theme)
    localStorage.setItem('theme', theme)
  }

  if (initialTheme) {
    rawSetTheme(initialTheme)
  }

  return (
    <ThemeContext.Provider value={{ theme, setTheme }}>
      {children}
    </ThemeContext.Provider>
  )
}

export default ThemeProvider
