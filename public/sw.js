/* eslint-disable no-restricted-globals */
/* eslint-disable no-undef */
importScripts(
  'https://storage.googleapis.com/workbox-cdn/releases/6.4.1/workbox-sw.js'
)

workbox.setConfig({ debug: true })

const { registerRoute } = workbox.routing
const { CacheFirst } = workbox.strategies
const { ExpirationPlugin } = workbox.expiration

// workbox.precaching.precacheAndRoute([{"revision":"c92b85a5b907c70211f4ec25e29a8c4a","url":"favicon.ico"},{"revision":"851a2070780a5734b407f9ba93a516f7","url":"flag-th-mask.png"},{"revision":"7101a006482b4c40d77e06e6093cd4d2","url":"flag-th.png"},{"revision":"bc8efad2cc8f9085d93822aca2b55d38","url":"flag-th.svg"},{"revision":"33dbdd0177549353eeeb785d02c294af","url":"logo192.png"},{"revision":"917515db74ea8d1aee6a246cfbcc0b45","url":"logo512.png"},{"revision":"97b80500a4370633e99b06bda9916309","url":"splashscreens/ipad_splash.png"},{"revision":"edd2daba51ae905e40ebca3d0acc5a73","url":"splashscreens/ipadpro1_splash.png"},{"revision":"6d839654150af7e9309835d240f14c08","url":"splashscreens/ipadpro2_splash.png"},{"revision":"3fde667e483baafcdc66a80a9150505f","url":"splashscreens/ipadpro3_splash.png"},{"revision":"4985f1c332c2344aee3831142c93a273","url":"splashscreens/iphone5_splash.png"},{"revision":"14518b5c1d2f801af4d5f39cd30cbfbb","url":"splashscreens/iphone6_splash.png"},{"revision":"5648d9fb63266c507aa62fb9da6b1e39","url":"splashscreens/iphoneplus_splash.png"},{"revision":"9bd2f7c09fc77e74eb30ea7e3bc0813e","url":"splashscreens/iphonex_splash.png"},{"revision":"e514ff6f098504dd2ee3b9b9a6ec6306","url":"splashscreens/iphonexr_splash.png"},{"revision":"21a28e7f2a4c1352c40d7b0fe927a062","url":"splashscreens/iphonexsmax_splash.png"}])
registerRoute(
  new workbox.routing.NavigationRoute(
    new CacheFirst({
      cacheName: 'pages',
      plugins: [new ExpirationPlugin({ maxAgeSeconds: 3600 })]
    })
  )
)

registerRoute(
  ({ request }) => request.destination === 'image',
  new CacheFirst({
    cacheName: 'images',
    plugins: [new ExpirationPlugin({ maxAgeSeconds: 3600 })]
  })
)

/* Types of Caching */
// new workbox.strategies.CacheFirst({})
// new workbox.strategies.CacheOnly({})
// new workbox.strategies.NetworkFirst({})
// new workbox.strategies.NetworkOnly({})
// new workbox.strategies.StaleWhileRevalidate({})
