/* eslint-disable no-restricted-globals */
/* eslint-disable no-undef */
importScripts('https://storage.googleapis.com/workbox-cdn/releases/6.4.1/workbox-window.prod.mjs')

workbox.setConfig({ debug: false })

workbox.precaching.precacheAndRoute(self.__WB_MANIFEST)
