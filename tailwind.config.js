/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        dark: {
          elem: '#2B3945',
          bg: '#202C37',
          text: '#FFFFFF',
          link: '#81E6D9'
        },
        light: {
          elem: '#FFFFFF',
          bg: '#FAFAFA',
          text: '#111517',
          link: '#2B6CB0',
          input: '#858585'
        },
        error: '#FF0909'
      }
    }
  },
  variants: {
    extend: {}
  },
  plugins: []
}
